/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import CameraScreen from './camera';
import {name as appName} from './app.json';

//AppRegistry.registerComponent(appName, () => CameraScreen);
AppRegistry.registerComponent(appName, () => App);
