## Application demonstrates use of native resources like 
## GPS and camera using RN functional components. 

- App.js contains GSP-functionality and camera.js contains
camera-functionality.

- In order to test, change the used component from index.js.
```
    import {AppRegistry} from 'react-native';
    import App from './App';
    import CameraScreen from './camera';
    import {name as appName} from './app.json';

    //AppRegistry.registerComponent(appName, () => App);
    AppRegistry.registerComponent(appName, () => CameraScreen);
```