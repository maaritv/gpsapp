/* eslint-disable prettier/prettier */
'use strict';
import React, { PureComponent, useState, useEffect, useRef } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Permissions from 'react-native-permissions';

export const CameraScreen = () => {
  let [flash, setFlash] = useState('off')
  let [type, setType] = useState('back')
  const [forbidden, setForbidden] = useState(false)
  const [message, setMessage] = useState('Picture is not taken yet');
  const [uri, setUri] = useState(null);
  const cameraRef = useRef(null)

  /**
 * https://www.npmjs.com/package/react-native-camera
 * npm i react-native-camera
 * npm i react-native-permissions
 * npx react-native link react-native-camera
 * Photos are saved to external storage (files can be shared with other apps)
 * Add this attribute to android/app/build.gradle (to defaultconfig) for application tag (element)
 * missingDimensionStrategy 'react-native-camera', 'general'
 * Add these lines to android/app/src/main/AndroidManifest.xml
 * <uses-permission android:name="android.permission.CAMERA" />
 * <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
*  <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
 */

  useEffect(() => {
    Permissions.check('photo').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'denied') {
        setForbidden(true);
        setMessage("Permission was not given for camera");
      }
      console.log(response);
    });
  }, []);


  function takePicture() {
    if (!forbidden) {
      const options = { quality: 0.5, base64: true };
      cameraRef.current.takePictureAsync(options).then(data => {
        setMessage(data.uri);
        setUri(data.uri);
      })
    }
    else {
      setMessage("Permission was not given");
    }
  };

  if (forbidden) {
    return (<SafeAreaView><Text>{message}</Text></SafeAreaView>);
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={{ fontSize: 30 }}>{message}</Text>
      <View style={{ height: '50%', backgroundColor: '#EEEEEE' }}>
        <RNCamera
          ref={cameraRef}
          style={styles.preview}
          type={type}
          flashMode={flash}
        />
      </View>
      <Text>Image is loaded from: {uri}</Text>
      <View style={{ height: '50%' }}>
        <TouchableOpacity onPress={() => takePicture()} style={styles.capture}>
          <Text style={{ fontSize: 14, color: '#FFFFFF' }}> SNAP </Text>
        </TouchableOpacity>
        {uri != null ? <Image style={styles.imageStyle} source={{ uri: uri }} /> : <Text>{message}</Text>}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  preview: {
    height: '40%',
    width: '100%',
  },
  capture: {
    width: 200,
    height: 50,
    backgroundColor: '#146965',
    margin: 50,
  },
  imageStyle: {
    width: 300,
    height: 200,
    alignSelf: 'center'
  }
})
export default CameraScreen;