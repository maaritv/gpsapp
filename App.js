/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import type { Node } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Button,
  View,
  TouchableOpacity,
} from 'react-native';
import RNLocation from 'react-native-location';
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message

/**
 * App uses npm package from:
 * https://www.npmjs.com/package/react-native-location
 * Run following command in terminal, in the project folder.
 * npm install --save react-native-location
 * npx react-native link react-native-location
 * Add this line to android/app/src/main/AndroidManifest.xml:
 * <uses-permission android:name="android.permission.ACCESSCOARSELOCATION"/>
 */


const App: () => Node = () => {
  const [location, setLocation] = useState({});
  const [forbidden, setForbidden] = useState(false);
  const [message, setMessage] = useState("");
  const [cnt, setCnt] = useState(0);


  function measure() {
    RNLocation.getLatestLocation({ timeout: 60000 })
      .then(latestLocation => {
        setLocation(latestLocation);
        setMessage("Measured " + new Date());
        setCnt(cnt + 1);
        console.log("measuring"); //Add console log, so that you know, when this function is executed
      })
      .catch(error=> {
        setMessage("Could not measure");
      })
  }


  useEffect(() => {
    console.log("configuring!");
    RNLocation.configure({
      distanceFilter: 100, // Meters
      desiredAccuracy: {
        ios: "best",
        android: "balancedPowerAccuracy"
      },
      // Android only
      androidProvider: "auto",
      interval: 1000, // Milliseconds
      fastestInterval: 1000, // Milliseconds
      maxWaitTime: 1000, // Milliseconds
      // iOS Only
      activityType: "other",
      allowsBackgroundLocationUpdates: false,
      headingFilter: 1, // Degrees
      headingOrientation: "portrait",
      pausesLocationUpdatesAutomatically: false,
      showsBackgroundLocationIndicator: false,
    })

    RNLocation.requestPermission({
      ios: "whenInUse",
      android: {
        detail: "coarse"
      }
    }).then(granted => {
      if (granted) {
        measure();
      }
      else {
        setMessage("Permission was not given.");
        setForbidden(true);
      }
    }).catch(error => {
      setMessage("Error happened!");
    })
  }, [])   //configuration is done only when app is started


  if (forbidden) {
    return (<SafeAreaView><Text>{message}</Text></SafeAreaView>);
  }

  //View is rendered when message, location or cnt state variable changes
  return (
    <SafeAreaView>
      <Text style={{fontSize: 20}}>Location was updated! {new Date(location.timestamp).toUTCString()}</Text>
      <Text>{message}  {cnt}</Text>
      <View style={{ marginTop: 10, padding: 10, borderRadius: 10, width: '90%' }}>
        <View style={{ height: 40, width: 100, backgroundColor: 'FF00FF' }}>
          <TouchableOpacity style={{ height: 40, width: 100, backgroundColor: '#123456' }}
            onPress={() => measure()}
          ><Text style={{ color: '#FFFFFF' }}>Measure!</Text></TouchableOpacity>
        </View>

        <Text style={{fontSize: 30}}>Latitude:{location.latitude}</Text>
        <Text style={{fontSize: 30}}>Longitude: {location.longitude}</Text>
        <Text style={{fontSize: 30}}>Speed: {location.speed}</Text>
        <Text>data: {JSON.stringify(location)}</Text>

        <Button
          title="Send Location"
        />
      </View>
      <View style={{ backgroundColor: '#007766', height: '60%' }}>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
